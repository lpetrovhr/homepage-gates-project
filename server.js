'use strict';

var express = require('express'),
  path = require('path'),
  app = express();

app.use('/src', express.static(__dirname + '/src'));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(process.env.PORT || 3000, function() {
  console.log('Example app listening on port ' + process.env.PORT || 3000);
});
