# Gates Homepage

Contains HTML/CSS code with respective JS code for parallax. Page is responsive.

## Development
Install module dependencies to allow gulp automated tool and also install Ruby gem to allow linting SCSS code:

    npm install
    gem install scss_lint
Gulpfile contains 3 tasks which we can run in root of project folder:

    gulp styles
    gulp styles-lint
    gulp watch-styles
The first command runs compilation of main.scss file in stylesheets and produces main.css file that we use on our page. It will also run the second command by default which lints all scss files. The third command watches files and recompiles main.css when any change upon any of scss files happens.
## Structure

    .
    ├── src
    |   ├── images
    |   ├── stylesheets
    |   └── videos
    ├── gulpfile.js
    └── index.html
Styles, images and videos are all in src folder. Styles are written in SASS (.scss sytnax) and if changed they need to be compiled to CSS. Respective images and video files are stored there. The main file is index.html that serves a page and loads styles, files and fonts.
## Dynamic content
All dynamic content can be applied inside index.html. There are examples in index.html for each possible variations.
### Video
Video is embedded to page through JS which is why we read video links from data attributes in HTML. From backend it is expected to have 4 data fileds populated which will JS read on load and embed dynamically. It is expected to have 3 video extensions and poster image. Those attributes are in header tag in index.html:

    <header id="js-video"
            class="header-banner"
            data-video-mp4="./src/videos/GF-HP-HeroVideo-720p.mp4"
            data-video-webm="./src/videos/GF-HP-HeroVideo-720p.webm"
            data-video-ogg="./src/videos/GF-HP-HeroVideo-720p.ogv"
            data-video-poster="./src/images/banner.jpg"
            data-mandarin="false">
    <div class="header-banner__cover" style="background-image: url('./src/images/banner.jpg')"></div>
Also, note that poster image has to be added to two places because on mobile phones we don't show video, we just use poster image.
### Mandarin version
As it can be seen from explanation up here, backend should send true/false flag to **data-mandarin** attribute in header tag which will be read from JS and if Mandarin is true it will disable video and parallax. It is important to note that overlay person image will not be removed through JS so it is important to add **.no-person** class to those images same as we add that class in case image is missing from backend.
### Video opacity
Video opacity can be dynamically changed. It is defined by a ticket that video should have a white overlay with 90% opacity. We changed that to 80% which is how it's done in PS by design. The reason behind that decision is that it is very hard to see the video if we make it per tickets description. Opacity should be added only on one place:

    <div class="header-banner--opacity" style="opacity: 0.8"></div>
So, to be able to dynamically change opacity it can be done by applying style attribute with specified opacity like in example above and if it's omitted it will use default opacity set to 0.8. It is important to note that we also add opacity on div with class **.header-banner__cover** which we show in case of mobile devices.
### Feature article
It is not important to send png image that is circled. We can expect any image and for optimization purposes it is better if it's jpeg as it's going to be smaller in size as opposed to png. On the frontend we will make circle around image. All that is needed to do is to put source to the image inside index.html on a div with class **.featured-article__image**:

    <div class="featured-article__image" style="background-image: url('./src/images/featured.jpg')"></div>
### Missions
Highly dynamic component that can be changed in several different ways.
#### Color
To be able to style it by color it is important to apply HEX color value returned from backend to multiple different elements. If these get omitted default color is blue. In example bellow and in index.html for mission component 2 it can be seen what elements need to be styled and how (there is couple of them):

    <img class="circle--background__mobile" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Background placeholder" style="background-color: #eb9700" />
    <div class="circle--background" style="background-color: #eb9700"></div>
    <h4 style="border-color: #eb9700">Especially women and girls, to transform their lifes</h4>
    <div class="divider divider--mission">
      <span class="divider__line--left" style="border-color: #eb9700"></span>
      <span class="divider__arrow divider__arrow--up" style="border-color: #eb9700"></span>
      <span class="divider__line--right" style="border-color: #eb9700"></span>
    </div>
    <strong style="color: #eb9700">That by giving people the tools to lead a healthy and productive life, they can lift themselves out of poverty.</strong>
    <strong style="background-color: #eb9700">Explore this mission</strong>
#### Person image
By ticket definition it is possible to omit person image. In that case several other elements on the page are going to act differently. In case person image is available on the page **.has-person** class needs to be applied to one element which can also be seen in index.html:

    <div class="mission-content__navigation navigation--right has-person">...</div>
If person image isn't available we need to remove **.has-person** class from above elements and add **.no-person** class to element that contains person image:

    <img class="mission-content__person no-person" src="./src/images/article3-person.png" alt="Person 1 Image"></div>
#### Header invert
By ticket definition title and subtitle of each mission can be inverted. In that case all that is needed is to add **.invert** class to both **h2** and **h4** elements:

    <h2 class="invert">To change the world</h2>
    <h4 class="invert">Inspire people to take action</h4>

#### Horizontal switch
Every ticket can be inverted horizontally so that content can be moved to another side. That can be achieved very easilly by adding a simple class **.mission-content--right** to the mission's article tag to invert content horizontally:

    <article class="mission-content mission-content--right">...</article>
