'use strict';

var gulp = require('gulp'),
  autoprefixer = require('gulp-autoprefixer'),
  sass = require('gulp-sass'),
  gutil = require('gulp-util'),
  scsslint = require('gulp-scss-lint'),
  eslint = require('gulp-eslint');

var paths = {
  scss: './src/stylesheets/**/*.scss',
  css: './src/stylesheets/**/*.css',
  excludeCss: '!./src/stylesheets/main.css',
  stylesheets: './src/stylesheets',
  js: '**/*.js',
  jsProject: './src/javascript/**/*.js',
  excludeNodeModules: '!node_modules/**',
  excludeVendor: '!./src/javascript/vendor/**/*.js'
};

gulp.task('styles-lint', function() {
  return gulp.src(paths.scss)
    .pipe(scsslint({
      config: '.scss-lint.yml'
    }));
});

gulp.task('styles', ['styles-lint'], function() {
  return gulp.src(paths.scss)
    .pipe(sass({
      onError: function(error) {
        gutil.log(gutil.colors.red(error));
        gutil.beep();
      },
      onSuccess: function() {
        gutil.log(gutil.colors.green('Sass styles compiled successfully.'));
      }
    }))
    .pipe(autoprefixer('last 2 versions', 'ie >= 10'))
    .pipe(gulp.dest(paths.stylesheets));
});

gulp.task('js-lint', function() {
  return gulp.src([paths.js, paths.excludeNodeModules, paths.excludeVendor])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('watch-styles', function() {
  gulp.watch([paths.scss, paths.css, paths.excludeCss], ['styles']);
});

gulp.task('watch-js', function() {
  gulp.watch([paths.js, paths.excludeNodeModules, paths.excludeVendor],
    ['js-lint']);
});

gulp.task('watch', ['watch-styles', 'watch-js']);
