'use strict';

(function () {
  var __MOBILE__ = 720,
    appended = false,
    scroll;

  function appendVideo(video) {
    if (appended) return;
    var videoElem = document.createElement('video'),
      sourceMp4 = document.createElement('source'),
      sourceWebm = document.createElement('source'),
      sourceOgg = document.createElement('source');

    videoElem.autoplay = true;
    videoElem.loop = true;
    videoElem.muted = true;
    videoElem.poster = video.getAttribute('data-video-poster');

    sourceMp4.type = 'video/mp4';
    sourceWebm.type = 'video/webm';
    sourceOgg.type = 'video/ogg';

    sourceMp4.src = video.getAttribute('data-video-mp4');
    sourceWebm.src = video.getAttribute('data-video-webm');
    sourceOgg.src = video.getAttribute('data-video-ogg');

    videoElem.appendChild(sourceMp4);
    videoElem.appendChild(sourceWebm);
    videoElem.appendChild(sourceOgg);
    video.appendChild(videoElem);
    videoElem.play();
    appended = true;
  }

  function parallaxAndVideo() {
    var video = document.getElementById('js-video'),
      mandarin = JSON.parse(video.getAttribute('data-mandarin'));
    if (mandarin || isMobile.any) return;

    var w = window.innerWidth || document.documentElement.clientWidth
      || document.body.clientWidth;

    if (w > __MOBILE__) {
      if (skrollr.get()) scroll.destroy();
      scroll = skrollr.init({
        forceHeight: false,
        smoothScrolling: true
      });
    } else if (skrollr.get()) {
      scroll.destroy();
    }

    appendVideo(video);
  }

  window.onload = parallaxAndVideo;
  window.onresize = parallaxAndVideo;
})();
